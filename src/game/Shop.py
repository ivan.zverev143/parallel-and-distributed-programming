from abc import ABC
from typing import Any

from core import keys
from core.IUObject import IUObject
from game.Deck import Deck


class Shop(IUObject, ABC):
    def __init__(self, deck: Deck):
        self.properties = dict()
        self.set_property(keys.DECK, deck)
        self.set_property(keys.SHOWCASE, [])

    def get_property(self, name: str) -> Any:
        return self.properties.get(name)

    def set_property(self, name: str, value: Any) -> None:
        self.properties[name] = value

    def get_showcase(self) -> list:
        return self.get_property(keys.DECK).  # TODO такой подход через проперти не позсоляет обращаться к атрибутам и методам возвращаемого класса, как это решить?
