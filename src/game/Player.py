from abc import ABC
from typing import Any

from core import keys
from core.IUObject import IUObject
from game.Hand import Hand
from game.Table import Table


class Player(IUObject, ABC):
    def __init__(self, hand: Hand, table: Table):
        self.properties = dict()
        self.set_property(keys.HAND, hand)
        self.set_property(keys.TABLE, table)
        self.set_property(keys.HP, 30)

    def get_property(self, name: str) -> Any:
        return self.properties.get(name)

    def set_property(self, name: str, value: Any) -> None:
        self.properties[name] = value
