from abc import ABC
from typing import Any

from core.IUObject import IUObject


class Table(IUObject, ABC):
    def __init__(self):
        self.properties = dict()

    def get_property(self, name: str) -> Any:
        return self.properties.get(name)

    def set_property(self, name: str, value: Any) -> None:
        self.properties[name] = value
