from abc import ABC
from typing import Any

from core import keys
from core.IUObject import IUObject
from game.Hand import Hand
from game.Table import Table
from game.Unit import Unit


class Deck(IUObject, ABC):
    def __init__(self, cards: [Unit]):  # можно ли так объявить массив
        self.properties = dict()
        self.set_property(keys.CARDS, cards)

    def get_property(self, name: str) -> Any:
        return self.properties.get(name)

    def set_property(self, name: str, value: Any) -> None:
        self.properties[name] = value

    def generate_showcase(self) -> [Unit]:
        if len(self.get_property(keys.CARDS)) < 5:
            raise Exception("Not enough cards in the deck!")  # TODO переписать exception

        showcase = self.get_property(keys.CARDS)[:5]
        rest = self.get_property(keys.CARDS)[5:]
        self.set_property(keys.CARDS, rest)
        return showcase
