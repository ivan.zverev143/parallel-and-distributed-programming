from abc import ABC
from typing import Any

from core import keys
from core.IUObject import IUObject


class Unit(IUObject, ABC):
    def __init__(self, hp, attack):
        self.properties = dict()
        self.set_property(keys.HP, hp)
        self.set_property(keys.ATTACK, attack)

    def get_property(self, name: str) -> Any:
        return self.properties.get(name)

    def set_property(self, name: str, value: Any) -> None:
        self.properties[name] = value
