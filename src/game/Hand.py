from abc import ABC
from typing import Any

from core import keys
from core.IUObject import IUObject


class Hand(IUObject, ABC):
    def __init__(self):
        self.properties = dict()
        self.set_property(keys.UNITS, [])

    def get_property(self, name: str) -> Any:
        return self.properties.get(name)

    def set_property(self, name: str, value: Any) -> None:
        self.properties[name] = value
