from abc import abstractmethod, ABC
from typing import List


class ICommand(ABC):
    @abstractmethod
    def execute(self) -> None:
        ...


class CommandException(Exception):
    def __init__(self, text):
        self.txt = text


class MacroCommand(ICommand):
    def __init__(self, commands: List[ICommand]):
        self.__commands = commands

    def execute(self) -> None:
        for c in self.__commands:
            try:
                c.execute()
            except CommandException as e:
                print(e)
                

class EmptyCommand(ICommand):
    def execute(self) -> None:
        ...