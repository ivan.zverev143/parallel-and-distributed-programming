import importlib
from abc import ABC, abstractmethod
from typing import Any

from core.command import ICommand, CommandException
from core.strategy import IStrategy


class IDependenciesContainer(ABC):
    @abstractmethod
    def __getitem__(self, key: str) -> IStrategy:
        ...

    @abstractmethod
    def __setitem__(self, key: str, strategy: IStrategy):
        ...


class IOCBaseResolveResolver(IStrategy):
    def __init__(self, container: IDependenciesContainer):
        self.__container = container

    def __call__(self, *args: Any, **kwargs: Any) -> IStrategy:
        key = args[0]
        return self.__container[key](*args[1:], **kwargs)


class IOCBaseRegisterCommandResolver(IStrategy):
    def __init__(self, container: IDependenciesContainer):
        self.__container = container

    def __call__(self, key: str, strategy: IStrategy) -> ICommand:
        try:
            return IOCBaseRegisterCommand(self.__container, key, strategy)
        except IndexError:
            raise ResolveDependencyException(
                "IoC.Register requires two args: key(str) and strategy(IStrategy)"
            )


class IOCBaseRegisterCommand(ICommand, ABC):
    def __init__(
        self,
        container: IDependenciesContainer,
        key: str,
        strategy: IStrategy,
    ):
        self.container = container
        self.key = key
        self.strategy = strategy

    def __call__(self) -> None:
        self.container[self.key] = self.strategy


class IOCBaseLoadPluginCommandResolver(IStrategy):
    def __call__(self, plugin_uri: str) -> Any:
        return IOCBaseLoadPluginCommand(plugin_uri)


class IOCBaseLoadPluginCommand(ICommand, ABC):
    def __init__(self, plugin_uri: str) -> None:
        self.__plugin_uri = plugin_uri

    def __call__(self) -> None:
        try:
            plugin = importlib.import_module(self.__plugin_uri)
        except ModuleNotFoundError:
            raise CommandException(f"Module {self.__plugin_uri} not found")

        # TODO: exceptions
        load_command = plugin.PluginLoadCommand()
        load_command()


class IOCBaseContainer(IDependenciesContainer):
    def __init__(self):
        self.__not_found_strategy = lambda key: raise_(
            ResolveDependencyException(f"Dependency {key} is missing")
        )
        self.__store = {"IoC.Resolve": IOCBaseResolveResolver(self),
                        "IoC.Register": IOCBaseRegisterCommandResolver(self), "IoC.BaseContainer": lambda: self,
                        "Plugin.Load": IOCBaseLoadPluginCommandResolver()}

    def __getitem__(self, key: str) -> IStrategy:
        try:
            return self.__store[key]
        except KeyError:
            return self.__not_found_strategy(key)
        except ResolveDependencyException as e:
            raise e

    def __setitem__(self, key: str, strategy: IStrategy):
        self.__store[key] = strategy


def raise_(ex):
    raise ex


ioc_base_container = IOCBaseContainer()


class ResolveDependencyException(Exception):
    ...


def resolve(key: str, *args, **kwargs) -> Any:
    try:
        return ioc_base_container["IoC.Resolve"](key, *args, **kwargs)
    except ResolveDependencyException as e:
        raise e
    except Exception as e:
        raise ResolveDependencyException(
            f"An unexpected exception occurred with key: {key} and args: {args}, and kwargs: {kwargs}: {e}"
        )