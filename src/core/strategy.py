from abc import abstractmethod, ABC
from typing import Any


class IStrategy(ABC):
    @abstractmethod
    def __call__(self, *args: Any) -> Any:
        ...
