from abc import ABC, abstractmethod
from typing import List


class Lamp:
    def __init__(self) -> None:
        self.__state = False

    def turn_on(self):
        self.__state = True

    def turn_off(self):
        self.__state = False

    def is_on(self):
        return self.__state


class ICommand(ABC):
    @abstractmethod
    def execute(self) -> None:
        ...


class TurnOnLamp(ICommand):
    def __init__(self, lamp: Lamp) -> None:
        self.__lamp = lamp

    def execute(self) -> None:
        self.__lamp.turn_on()


class TurnOffLamp(ICommand):
    def __init__(self, lamp: Lamp) -> None:
        self.__lamp = lamp

    def execute(self) -> None:
        self.__lamp.turn_off()


class SimpleMacroCommand(ICommand):
    def __init__(self, commands: List[ICommand]):
        self.__commands = commands

    def execute(self) -> None:
        for c in self.__commands:
            c.execute()
            print(c)


class GameQueue:
    def __init__(self) -> None:
        self.__store = []

    def push(self):
        ...

    def pop(self):
        ...

    def run(self):
        ...