import unittest
import src.example


class TestExampleTestCase(unittest.TestCase):
    def test_first(self):
        self.assertEqual(5+5, 10)

    def test_lamp2(self):
        lamp = src.example.Lamp()
        turn_on_command = src.example.TurnOnLamp(lamp)
        turn_off_command = src.example.TurnOffLamp(lamp)

        commands: src.example.List[src.example.ICommand] = [
            turn_on_command, turn_off_command, turn_on_command, turn_off_command
        ]

        for c in commands:
            c.execute()
            print(lamp.is_on())

