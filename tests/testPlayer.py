import unittest

from core import keys
from game.Deck import Deck
from game.Hand import Hand
from game.Player import Player
from game.Table import Table


class TestPlayer(unittest.TestCase):
    def test_create(self):
        hand = Hand()
        table = Table()
        player = Player(hand, table)
        self.assertEqual(player.get_property(keys.HP), 30)
        self.assertEqual(player.get_property(keys.HAND), hand)
        self.assertEqual(player.get_property(keys.TABLE), table)
